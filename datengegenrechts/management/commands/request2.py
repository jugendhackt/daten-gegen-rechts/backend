import json
import requests
import re
from ...models import *
from django.core.management.base import BaseCommand
from datetime import datetime
from django.utils import timezone

class Command(BaseCommand):
    def handle(self, *args, **options):
        r = requests.get('https://raw.githubusercontent.com/ax3l/chronik-vorfaelle/data/vorfaelle.geojson')
        r = r.json()
        out = {}
    
        i = 0
        for x in r['features']:
            time = datetime.strptime(x['properties']['date'], '%d.%m.%Y')
            time = timezone.make_aware(time)

            if len(x['properties']['source_href']) > 0:
                url = x['properties']['source_href'][0]
            else:
                url = None

            out = {
                'timestamp': time,
                'title':x['properties']['title'],
                'description': x['properties']['description'],
                'location': {
                    'lat': x['geometry']['coordinates'][1],
                    'lon': x['geometry']['coordinates'][0],
                    'city': x['properties']['city'],
                    'state': x['properties']['state']
                },
                'motive': '',
                'sources': [{
                    'name': x['properties']['source'],
                    'url': url
                }]
            }
            
            try:
                mot = Motive.objects.get(name=out['motive'])
            except:
                mot = Motive.objects.create(name=out['motive'])
            try:
                loc = Location.objects.get(name=out['location']['city'])
            except:
                loc = Location.objects.create(name=out['location']['city'],lat=out['location']['lat'],lon=out['location']['lon'],city=out['location']['city'],state=out['location']['state'])
                print(out['location']['city'])
                loc = Location.objects.get(name=out['location']['city'])
            try:
                ori = Origin.objects.get(name='ax3l')
            except:
                ori = Origin.objects.create(name='ax3l', url='https://raw.githubusercontent.com/ax3l/chronik-vorfaelle/data/vorfaelle.geojson')

            try:
                inc = Incident.objects.get(description=out['description'])
            except:
                inc = Incident.objects.create(timestamp=out['timestamp'],title=out['title'],description=out['description'],motive=mot,location=loc,origin=ori)

            try:
                src = Source.objects.get(url=out['sources'][0]['url'])
            except:
                src = Source.objects.create(name=out['sources'][0]['name'],url=out['sources'][0]['url'],incident=inc)
            i += 1
            print(i)
