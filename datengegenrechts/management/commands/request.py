import json
import requests
import re
from datetime import datetime
from django.utils import timezone
from django.core.management.base import BaseCommand
from ...models import *
from time import sleep
from geopy.geocoders import Nominatim

class Command(BaseCommand):
    def handle(self, *args, **options):
        i = 0

        gloc = Nominatim(user_agent="https://gitlab.com/jugendhackt/daten-gegen-rechts/backend/", domain="nominatim-cache.dont-break.it")

        r = requests.get('https://angstraeume.ezra.de/wp-json/ezra/v1/chronic')
        r = r.json()
        out = {}

        for x in r['entries']:
            if x['motives'] == []:
                x['motives'] = {0: 12}
            if x['locations'] != []:
                loc = gloc.geocode(r['meta']['locations'][str(x['locations'][0])].replace('Stadt',''))
                lat = loc.latitude
                long= loc.longitude
            else:
                lat = 0
                long= 0
            try:
                time = datetime.strptime(x['startDisplay'], '%d.%m.%Y')
            except:
                time = datetime.strptime('1.1.1970', '%d.%m.%Y')

            time = timezone.make_aware(time)

            out = {
                'timestamp': time,
                'title': x['title'],
                'description': re.sub(re.compile('<.*?>'),'',re.sub(re.compile('&.*;'), '',x['content'])).replace('\n',''),
                'location': {
                    'lat': lat,
                    'lon': long,
                    'city': x['locationDisplay'],
                    'state': 'Thuringen'
                },
                'motive': r['meta']['motives'][str(x['motives'][0])],
                'sources': [{
                    'name': x['sourceName'],
                    'url': x['sourceUrl']
                }]
            }

            try:
                mot = Motive.objects.get(name=out['motive'])
            except:
                mot = Motive.objects.create(name=out['motive'])
            try:
                loc = Location.objects.get(name=out['location']['city'])
            except:
                loc = Location.objects.create(name=out['location']['city'],lat=out['location']['lat'],lon=out['location']['lon'],city=out['location']['city'],state=out['location']['state'])
            try:
                ori = Origin.objects.get(name='Ezra')
            except:
                ori = Origin.objects.create(name='Ezra', url='https://ezra.de/chronik/')

            try:
                inc = Incident.objects.get(description=out['description'])
            except:
                inc = Incident.objects.create(timestamp=out['timestamp'],title=out['title'],description=out['description'],motive=mot,location=loc,origin=ori)

            try:
                src = Source.objects.get(url=out['sources'][0]['url'])
            except:
                src = Source.objects.create(name=out['sources'][0]['name'],url=out['sources'][0]['url'],incident=inc)
            i += 1
            print(i)
            sleep(1)
