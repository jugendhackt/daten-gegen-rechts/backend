from django.shortcuts import get_object_or_404, render
from django.forms.models import model_to_dict
from django.core import serializers
from django.http import HttpResponse,JsonResponse,Http404
from .models import *
from .serializers import *
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status,generics,mixins,filters
from django.views.decorators.cache import cache_page
import django_filters.rest_framework

class IncidentList(generics.ListAPIView):
    """
    List all incidents
    """
    queryset = Incident.objects.all()
    serializer_class = IncidentListSerializer
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter]
    filterset_fields = ['id', 'title', 'description', 'timestamp', 'motive', 'sources', 'origin']
    search_fields = ['title', 'description', 'timestamp', 'motive__name', 'sources__name', 'sources__url', 'origin__name']
    ordering_fields = ['id', 'timestamp']

class IncidentDetail(generics.RetrieveAPIView):
    """
    Get specific incident
    """
    queryset = Incident.objects.all()
    serializer_class = IncidentSerializer

class MotiveList(generics.ListAPIView):
    """
    List all Motives
    """
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter]
    filterset_fields = '__all__'
    search_fields = ['id', 'name']
    ordering_fields = '__all__'
    queryset = Motive.objects.all()
    serializer_class = MotiveSerializer

class MotiveDetail(generics.RetrieveAPIView):
    """
    Get specific Motive
    """
    queryset = Motive.objects.all()
    serializer_class = MotiveSerializer

class OriginList(generics.ListAPIView):
    """
    List all origins
    """
    queryset = Origin.objects.all()
    serializer_class = OriginSerializer

class OriginDetail(generics.RetrieveAPIView):
    """
    Get specific origin
    """
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter]
    filterset_fields = '__all__'
    search_fields = '__all__'
    ordering_fields = '__all__'
    queryset = Origin.objects.all()
    serializer_class = OriginSerializer

def index(request):
    return HttpResponse('Hello World')
