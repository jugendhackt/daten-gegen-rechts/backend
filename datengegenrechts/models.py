from urllib.parse import urlparse
from django.db import models

class Location(models.Model):
    name = models.CharField(max_length=255)
    lat = models.FloatField()
    lon = models.FloatField()
    city = models.CharField(max_length=255)
    state = models.CharField(max_length=255)
    parent = models.ForeignKey('self', related_name='childs', on_delete=models.PROTECT, blank=True, null=True)

    def __str__(self):
        return self.name

class Motive(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class Incident(models.Model):
    timestamp = models.DateTimeField()
    title = models.CharField(max_length=255)
    description = models.TextField(default='')
    motive = models.ForeignKey('Motive', on_delete=models.PROTECT, related_name='incident')
    location = models.ForeignKey('Location', on_delete=models.PROTECT, null=True, related_name='incident')
    origin = models.ForeignKey('Origin', on_delete=models.PROTECT, blank=True, null=True, related_name='incident')

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['timestamp']

class Source(models.Model):
    name = models.CharField(max_length=255)
    url = models.URLField(max_length=511, null=True, blank=True)
    timestamp = models.DateTimeField(null=True)
    incident = models.ForeignKey('Incident', on_delete=models.PROTECT, related_name='sources')

    def __str__(self):
        return self.name

    @property
    def hostname(self):
        if not self.url:
            return "-"

        return urlparse(self.url).hostname.replace('www.','')


class Origin(models.Model):
    name = models.CharField(max_length=255)
    url = models.URLField(max_length=511, blank=True, null=True)
    license = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name

