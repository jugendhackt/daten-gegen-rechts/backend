from django.urls import path, re_path
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.documentation import include_docs_urls
from rest_framework.schemas import get_schema_view
from rest_framework.renderers import OpenAPIRenderer, JSONOpenAPIRenderer
from django.views.generic import TemplateView
from django.views.decorators.cache import cache_page
from . import views

incidents = views.IncidentList.as_view()
incidents = cache_page(60*15)(incidents)
motives = views.MotiveList.as_view()
motives = cache_page(60*15)(motives)
origins = views.IncidentList.as_view()
origins = cache_page(60*15)(origins)

schema_view = get_schema_view(
    title="Daten gegen rechts API",
    renderer_classes=[JSONOpenAPIRenderer, ]
)

urlpatterns = [
    path('', views.index, name='index'),
    re_path(r'incidents/?$', incidents),
    path('incidents/<int:pk>', views.IncidentDetail.as_view()),
    re_path(r'motives/?$', motives),
    path('motives/<int:pk>', views.MotiveDetail.as_view()),
    re_path(r'origins/?$', origins),
    path('origins/<int:pk>', views.OriginDetail.as_view()),
    re_path('^schema$', schema_view, name='openapi-schema'),
    path('documentation/', TemplateView.as_view(template_name="swagger.html", extra_context={'schema_url':'openapi-schema'}))
]

urlpatterns = format_suffix_patterns(urlpatterns)
