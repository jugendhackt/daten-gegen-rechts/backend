from django.apps import AppConfig


class DatengegenrechtsConfig(AppConfig):
    name = 'datengegenrechts'
