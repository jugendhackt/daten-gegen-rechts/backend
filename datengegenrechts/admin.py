from django.contrib import admin

from .models import *

admin.site.register([Incident, Source, Motive, Location, Origin])

# Register your models here.
