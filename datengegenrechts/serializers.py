from rest_framework import serializers
from .models import *

class SourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Source
        fields = ['name', 'url', 'hostname']

class LocationListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = ['lat', 'lon']

class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = ['lat', 'lon', 'city', 'state']

class MotiveSerializer(serializers.ModelSerializer):
    class Meta:
        model = Motive
        fields = '__all__'

class OriginSerializer(serializers.ModelSerializer):
    class Meta:
        model = Origin
        fields = '__all__'
        
class IncidentListSerializer(serializers.ModelSerializer):
    location = LocationListSerializer()

    class Meta:
        model = Incident
        fields = '__all__'
        #fields = ['id', 'location', 'timestamp']

class IncidentSerializer(serializers.ModelSerializer):
    sources = SourceSerializer(many=True)
    location = LocationSerializer()
    motive = MotiveSerializer()
    origin = OriginSerializer()

    class Meta:
        model = Incident
        fields = '__all__'

